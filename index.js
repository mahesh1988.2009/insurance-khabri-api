const express = require('express');
const bodyParser = require('body-parser');
const api = require('./routes/api');
const cors = require('cors');
const app = express();
const config = require('./config');

app.use(cors());
app.use(bodyParser.json());
app.use('/api', api);
app.get('/', function(req,res){
  res.send("Hello from server"); 
});

app.listen(config.PORT, function(){
  console.log('Server is running on : ', config.PORT);
})