const sendVerifyMail = require('./verifymail');
const jwt = require('jsonwebtoken');
const User = require('./../models/user');

registerUser = async (req, res) =>{
  let userData = req.body;
  let user = new User(userData);
  user.save((error, registeredUser) => {
    try {
      if(error) {
        if(error.name === 'MongoServerError') {
          if(error.code === 11000 && error.keyPattern.email === 1) {
            res.status(409).send({
              message: 'Accound already exist with this email'
            });
          }
        }
        else {
          res.status(403).send({
            message: error
          });
        }
      }
      else {
        let payload = { subject: registeredUser._id };
        let token = jwt.sign(payload, 'secretKey');
        sendVerifyMail(registeredUser.name, registeredUser.email, registeredUser._id);
        res.status(200).send({
          message: "User registered successfully",
          userData,
          token,
        });
      }
    }
    catch (err) {
      res.json(err.errors.message);
    }
  });
}

module.exports = registerUser;