var bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('./../models/user');

loginUser = async (req, res, err) =>{
  let userData = req.body;
  User.findOne({email: userData.email}, async (error, user) => {
    try {
      if(error) {
        console.log(error);
      }
      else {
        if(!user) {
          res.status(401).send('Invalid email');
        }
        if (user) {
          let isPasswordValid = await bcrypt.compare(userData.password, user.password);
          if (isPasswordValid) {
            let payload = { subject: user._id };
            let token = jwt.sign(payload, 'secretKey');
            res.status(200).send({token});
          }
          else {
            res.status(401).send('Invalid password');
          }
        }
      }
    }
    catch (err) {
      res.json(err);
    }
  });
}

module.exports = loginUser;