const User = require('./../models/user');

verifyEmail = async (req, res) => {
  try {
    const updateVerifiedStatus = await User.updateOne({_id: req.query.id}, {$set: { isVerified: true }});
    res.status(200).send({
      message: "User verfied successfully!"
    });
  } catch (error) {
    console.log("Error:", error);
  }
}

module.exports = verifyEmail;