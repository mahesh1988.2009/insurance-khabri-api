const nodemailer = require('nodemailer');
const config = require('./../config');
// for sending verificationemail
sendVerifyMail = async (name, email, id) => {
  try {
    // connect with smtp
    let transporter = await nodemailer.createTransport({
      host: 'smtp.sendgrid.net', //smtp.gmail.com
      port: 465,
      secure: true,
      auth: {
        user: 'apikey', //mshnodemailer@gmail.com
        pass: config.SEND_GRID_KEY // config.NODEMAILER_PASSWORD
      }
    });

    await transporter.sendMail({
      from: '"Mahesh" <mshnodemailer@gmail.com>', // sender address
      to: email, // list of receivers
      subject: "Insurance Khabri Verification", // Subject line
      html: '<p>Hi ' + name + '<br/>Please click <a href="http://localhost:3000/verify-email?id='+id+'">here</a> to verify your email address</p>', // html body
    }, function(error, log) {
      if(error) {
        console.log(error);
      }
      else {
        res.status(200).send({
          log,
          message: "Verification mail sent successfully!"
        });
      }
    });
  } catch (error) {
    console.log(error.message);
  }
}

module.exports = sendVerifyMail;