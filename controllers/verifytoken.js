const jwt = require('jsonwebtoken');

function verifyToken(req, res, next) {
  if(!req.headers.authorization) {
    return res.status(401).send('Unauthorized request')
  }
  let token = req.headers.authorization.split(' ')[1];
  if(token === 'null') {
    return res.status(401).send('Unauhrized request');
  }
  let payload = jwt.verify(token, 'secretKey');
  if(!payload) {
    return res.status(401).send('Unauhrized requestd');
  }
  req.userId = payload.subject;
  next();
}

module.exports = verifyToken;