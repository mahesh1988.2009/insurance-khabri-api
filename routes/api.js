const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const verifyEmail = require('./../controllers/verifymail');
const verifyToken = require('./../controllers/verifytoken')
const loginUser = require('./../controllers/loginuser');
const registerUser = require('./../controllers/register');
const config = require('./../config');

mongoose.connect(config.DATABASE_KEY, err => {
  if(err) {
    console.error('Error!', err);
  }
  else {
    console.log('Connected to mongodb');
  }
});

router.get('/', (req, res)=> {
  res.send('From api route');
});

router.post('/register', registerUser);

router.get('/verify-email', verifyEmail);

router.post('/login', loginUser);

router.get('/blogs', verifyToken, (req, res) => {
  const events = [
    {
      "_id": "1",
      "name": "auto expo",
      "description": "lorem ipsum",
      "date": "2012-04-23T18:25:43.511Z"
    },
    {
      "_id": "2",
      "name": "auto expo",
      "description": "lorem ipsum",
      "date": "2012-04-23T18:25:43.511Z"
    },
    {
      "_id": "3",
      "name": "auto expo",
      "description": "lorem ipsum",
      "date": "2012-04-23T18:25:43.511Z"
    },
    {
      "_id": "4",
      "name": "auto expo",
      "description": "lorem ipsum",
      "date": "2012-04-23T18:25:43.511Z"
    }
  ];
  res.json(events);
});

module.exports = router;