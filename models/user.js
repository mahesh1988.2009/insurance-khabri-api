const mongoose = require('mongoose');
const emailValidator = require('email-validator');
const Schema = mongoose.Schema;
var bcrypt = require('bcrypt');

const userSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Name can not be empty']
  },
  email : {
    type:String,
    required:[true, 'Email is required' ],
    unique:[true, 'Already exist'],
    validate: [function() {
      return emailValidator.validate(this.email);
    }, 'Email is invalid']
  },
  phone: {
    type: Number,
    required: false
  },
  password: {
    type:String,
    required:[true, 'Password is required'],
    minLength: [8, 'Password should be minimum of 8 characters']
  },
  cnfPassword: {
    type:String,
    required:[true, 'Confirm password is required'],
    minLength:8,
    validate: [function() {
      return this.cnfPassword == this.password
    }, 'Confirm password and password should be same']
  },
  role: {
    type:String,
    enum:['admin','user'],
    default: 'user'
  },
  isVerified: {
    type: Boolean,
    default: false,
  },
  profileImage: {
    type: String,
    default: 'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.istockphoto.com%2Fphotos%2Fuser-profile&psig=AOvVaw2hwMJ4GSnQjVfHIiTpoGuY&ust=1676376097694000&source=images&cd=vfe&ved=0CAwQjRxqFwoTCMCs79-5kv0CFQAAAAAdAAAAABAE'
  },
  oldPassword: {
    type: String,
    default: '',
  },
});

userSchema.pre('save', function (next) {
  var user = this;
  bcrypt.genSalt(10, function(err, salt) {
      if (err) return next(err);
      bcrypt.hash(user.password, salt, async function(err, hash) {
        if (err) return next(err);
        user.password = await hash;
        user.verify = await hash;
        next();
      });
  });
  this.cnfPassword = undefined;
})

module.exports = mongoose.model('user', userSchema, 'users')